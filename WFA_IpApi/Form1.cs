﻿using System;
using System.Net.Http;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace WFA_IpApi
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private async void buttonGo_Click(object sender, EventArgs e)
        {
            buttonGo.Enabled = false;
            var ip = "";
            var key = "?access_key=2dfff1a3c8324f5efc1d78c6a9b9dd29";
            var url = "";

            if (ipAddressControlTool.Text == "...")
                ip = "check";
            else ip = ipAddressControlTool.Text;

            url = $"http://api.ipstack.com/{ip}{key}";


            //Dictionary<string, string> requestParams = new Dictionary<string, string>();
            //requestParams["ip"] = ip;
            //FormUrlEncodedContent encodedRequestParams = new FormUrlEncodedContent(requestParams);
            //var responce = await httpClient.PostAsync(url, encodedRequestParams);

            var httpClient = new HttpClient();

            var serverResponse = await httpClient.GetStringAsync(url);
            var json = JObject.Parse(serverResponse);

            richTextBoxAnswer.Text = json["ip"] + "\n" + json["type"] + "\n" + json["continent_code"] + "\n" +
                                     json["continent_name"] + "\n" + json["country_code"] + "\n" +
                                     json["country_name"] + "\n" + json["region_code"] + "\n" + json["region_name"] +
                                     "\n" + json["city"] + "\n" + json["zip"] + "\n" + json["latitude"] + "\n" +
                                     json["longitude"];

            buttonGo.Enabled = true;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ipAddressControlTool.Clear();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
        }
    }
}