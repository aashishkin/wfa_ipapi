﻿namespace WFA_IpApi
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxAnswer = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonGo = new System.Windows.Forms.Button();
            this.richTextBoxKey = new System.Windows.Forms.RichTextBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.ipAddressControlTool = new IPAddressControlLib.IPAddressControl();
            this.SuspendLayout();
            // 
            // richTextBoxAnswer
            // 
            this.richTextBoxAnswer.HideSelection = false;
            this.richTextBoxAnswer.Location = new System.Drawing.Point(145, 107);
            this.richTextBoxAnswer.Name = "richTextBoxAnswer";
            this.richTextBoxAnswer.ReadOnly = true;
            this.richTextBoxAnswer.Size = new System.Drawing.Size(139, 167);
            this.richTextBoxAnswer.TabIndex = 3;
            this.richTextBoxAnswer.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Информация об IP адресе:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Введите IP адрес:";
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(117, 29);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(171, 49);
            this.buttonGo.TabIndex = 2;
            this.buttonGo.Text = "Запросить информацию";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // richTextBoxKey
            // 
            this.richTextBoxKey.Enabled = false;
            this.richTextBoxKey.HideSelection = false;
            this.richTextBoxKey.Location = new System.Drawing.Point(12, 107);
            this.richTextBoxKey.Name = "richTextBoxKey";
            this.richTextBoxKey.ReadOnly = true;
            this.richTextBoxKey.Size = new System.Drawing.Size(132, 167);
            this.richTextBoxKey.TabIndex = 3;
            this.richTextBoxKey.Text = "IP адрес:\nТип соединения:\nКод континента:\nНазвание континента:\nКод страны:\nСтрана" +
    ":\nКод региона:\nОбласть:\nГород:\nZip код:\nШирота:\nДолгота:";
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(16, 55);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(95, 23);
            this.buttonClear.TabIndex = 4;
            this.buttonClear.Text = "Очистить";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // ipAddressControlTool
            // 
            this.ipAddressControlTool.AllowInternalTab = false;
            this.ipAddressControlTool.AutoHeight = true;
            this.ipAddressControlTool.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControlTool.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControlTool.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControlTool.Location = new System.Drawing.Point(16, 29);
            this.ipAddressControlTool.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControlTool.Name = "ipAddressControlTool";
            this.ipAddressControlTool.ReadOnly = false;
            this.ipAddressControlTool.Size = new System.Drawing.Size(95, 20);
            this.ipAddressControlTool.TabIndex = 5;
            this.ipAddressControlTool.Text = "...";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 286);
            this.Controls.Add(this.ipAddressControlTool);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBoxKey);
            this.Controls.Add(this.richTextBoxAnswer);
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IP info";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxAnswer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.RichTextBox richTextBoxKey;
        private System.Windows.Forms.Button buttonClear;
        private IPAddressControlLib.IPAddressControl ipAddressControlTool;
    }
}

